﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.OleDb;

namespace DataGrid
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        int id;
        int old_id;
        string conn_param = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + @"БД.mdb";
        char c;
        char lastchar=' ';

        //Вывод данных с бд
        private void FromData()
        {
            try
            {

                OleDbConnection connection = new OleDbConnection(conn_param);
                OleDbCommand command = connection.CreateCommand();
                command.CommandText = "select Код, Фамилия, Имя, Отчество, Телефон from Владельцы";
                connection.Open();
                OleDbDataReader reader = command.ExecuteReader();

                int i = 0;
                try
                {
                    while (reader.Read())
                    {
                        dataGridView1.Rows.Add();
                        dataGridView1.Rows[i].ReadOnly = true;
                        dataGridView1[0, i].Value = reader.GetInt32(0);
                        try { dataGridView1[1, i].Value = reader.GetString(1); } catch { dataGridView1[2, i].Value = " "; }
                        try { dataGridView1[2, i].Value = reader.GetString(2); } catch { dataGridView1[2, i].Value = " "; }
                        try { dataGridView1[3, i].Value = reader.GetString(3); } catch { dataGridView1[3, i].Value = " "; }
                        try { dataGridView1[4, i].Value = reader.GetString(4); } catch { dataGridView1[4, i].Value = " "; }
                        ++i;
                    }
                    dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);
                }
                finally
                {
                    reader.Close();
                    connection.Close();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void ActivateButton(bool b1, bool b2, bool b3, bool b4, bool b5)
        {
            button1.Enabled = b1;
            button2.Enabled = b2;
            button3.Enabled = b3;
            btnCancel.Enabled = b4;
            btnOK.Enabled = b5;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {

            dataGridView1.Columns.Add("Kod", "Код");
            dataGridView1.Columns.Add("Sername", "Фамилия");
            dataGridView1.Columns.Add("Name", "Имя");
            dataGridView1.Columns.Add("Secondname", "Отчество");
            dataGridView1.Columns.Add("Phone", "Телефон");

            FromData();
        }


        /*------------------------------------------------Кнопки --------------------------------------------------*/

        //Добавление
        private void button1_Click(object sender, EventArgs e)
        {

            int i = dataGridView1.Rows.Count - 1;
            DataGridViewRow newRow = new DataGridViewRow();

            dataGridView1.Sort(dataGridView1.Columns[0], ListSortDirection.Ascending);
            dataGridView1.Rows.Add(newRow);


            if (i != 0) { dataGridView1[0, i].Value = Convert.ToInt32(dataGridView1[0, i - 1].Value) + 1; } else { dataGridView1[0, i].Value = 1; }
            dataGridView1.Rows[i].ReadOnly = false;
            dataGridView1[1, i].Value = txtSurname.Text;
            dataGridView1[2, i].Value = txtName.Text;
            dataGridView1[3, i].Value = txtSecondName.Text;
            dataGridView1[4, i].Value = txtNumber.Text;
            dataGridView1.Rows[i + 1].ReadOnly = true;

            id = dataGridView1.Rows.Count - 2;

            updat.Visible = true;
            ActivateButton(false, false, false, true, true);
            c = 'I';
        }

        //Изменение
        private void button2_Click(object sender, EventArgs e)
        {
            int Index = dataGridView1.CurrentRow.Index;
            dataGridView1.Rows[Index].Selected = true;

            try
            {
                DialogResult dg = MessageBox.Show("Была выделена строка под номером " + dataGridView1[0, Index].Value.ToString() + "\nВы уверены что хотите ИЗМЕНИТЬ именно эту строку?", "Предупреждение", MessageBoxButtons.YesNo);

                dataGridView1.Rows[Index].DefaultCellStyle.BackColor = Color.LightBlue;

                if (dg == DialogResult.Yes)
                {
                    old_id = Convert.ToInt32(dataGridView1[0, Index].Value);
                    dataGridView1.Rows[Index].ReadOnly = false;

                    if (txtSurname.Text != "") { dataGridView1[1, Index].Value = txtSurname.Text; }
                    if (txtName.Text != "") { dataGridView1[2, Index].Value = txtName.Text; }
                    if (txtSecondName.Text != "") { dataGridView1[3, Index].Value = txtSecondName.Text; }
                    if (txtNumber.Text != "") { dataGridView1[4, Index].Value = txtNumber.Text; }

                    id = Index;
                    updat.Visible = true;
                    ActivateButton(false, false, false, true, true);
                    c = 'U';
                }
                else
                {
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Выбрана строка без кодового ключа" + "\nПопробуйте выделить строку, которая уже содержит код", "Ошибка");
            }


        }


        //Удаление
        private void button3_Click(object sender, EventArgs e)
        {
            int Index = dataGridView1.CurrentRow.Index;
            dataGridView1.Rows[Index].Selected = true;

            try
            {
                DialogResult dg = MessageBox.Show("Была выделена строка под номером " + dataGridView1[0, Index].Value.ToString() + "\nВы уверены что хотите УДАЛИТЬ именно эту строку?", "Предупреждение", MessageBoxButtons.YesNo);

                dataGridView1.Rows[Index].DefaultCellStyle.BackColor = Color.LightBlue;

                if (dg == DialogResult.Yes)
                {
                    id = Index;

                    ActivateButton(false, false, false, true, true);
                    c = 'D';
                }
                else
                {
                    return;
                }
            }
            catch
            {
                MessageBox.Show("Выбрана строка без кодового ключа" + "\nПопробуйте выделить строку, которая уже содержит код", "Ошибка");
            }
        }

        //Отменить
        private void button5_Click(object sender, EventArgs e)
        {
            ActivateButton(true, true, true, false, false);
            dataGridView1.Rows.Clear();
            FromData();
        }

        //Подтверждение
        private void button4_Click(object sender, EventArgs e)
        {


            int ID = Convert.ToInt32(dataGridView1[0, id].Value);
            string surname = Convert.ToString(dataGridView1[1, id].Value);
            string name = Convert.ToString(dataGridView1[2, id].Value);
            string secondname = Convert.ToString(dataGridView1[3, id].Value);
            string number = Convert.ToString(dataGridView1[4, id].Value);


            surname.Trim();
            name.Trim();
            secondname.Trim();
            number.Trim();

            try
            {
                OleDbConnection connection = new OleDbConnection(conn_param);
                OleDbCommand command = connection.CreateCommand();
                try
                {
                    switch (c)
                    {
                        case 'I':
                            command.CommandText = String.Format("INSERT INTO Владельцы (Код, Фамилия, Имя, Отчество, Телефон) VALUES ('{0}','{1}','{2}','{3}','{4}');", ID, surname, name, secondname, number);
                            connection.Open();
                            command.ExecuteNonQuery();
                            MessageBox.Show("В таблицу были добавлены следующие данные: " + String.Format(" '{0}', '{1}', '{2}', '{3}', '{4}' ", ID, surname, name, secondname, number));
                            break;
                        case 'U':
                            command.CommandText = String.Format("UPDATE Владельцы SET Код = {0}, Фамилия = '{1}', Имя ='{2}', Отчество = '{3}', Телефон = '{4}' WHERE Код = {5}", ID, surname, name, secondname, number, old_id);
                            try
                            {
                                connection.Open();
                                command.ExecuteNonQuery();
                                MessageBox.Show("В таблицы были изменены данные " + String.Format("UPDATE Владельцы SET Код = {0}, Фамилия = '{1}', Имя ='{2}', Отчество = '{3}', Телефон = '{4}' WHERE Код = {5}", ID, surname, name, secondname, number, old_id), "Внимание");
                            }
                            catch (Exception ex)
                            { MessageBox.Show(ex.Message); }
                            break;
                        case 'D':
                            command.CommandText = String.Format("DELETE FROM Владельцы WHERE Код = {0}", ID);
                            connection.Open();
                            command.ExecuteNonQuery();
                            MessageBox.Show("Удалена таблица с данными: " + String.Format(" '{0}', '{1}', '{2}', '{3}', '{4}' ", ID, surname, name, secondname, number));
                            break;
                        default:
                            break;
                    }

                    updat.Visible = false;
                    ActivateButton(true, true, true, false, false);
                    dataGridView1.Rows.Clear();
                    FromData();
                }
                finally
                {
                    connection.Close();
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void btnClearAll_Click(object sender, EventArgs e)
        {
            txtName.Clear();
            txtNumber.Clear();
            txtSecondName.Clear();
            txtSurname.Clear();
        }

        private void txtNumber_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((!(Char.IsDigit(e.KeyChar)) && !(e.KeyChar == '-')) || ((e.KeyChar == lastchar) && (e.KeyChar == '-')))
            {

                if (e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }

            }
            lastchar = e.KeyChar;

        }

        private void dataGridView1_EditingControlShowing(object sender, DataGridViewEditingControlShowingEventArgs e)
        {
            TextBox tb = (TextBox)e.Control;
            tb.KeyPress -= tb_KeyPress;
            tb.KeyPress += new KeyPressEventHandler(this.tb_KeyPress);
        }

        void tb_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (dataGridView1.CurrentCell.ColumnIndex)
            {
                case 0:
                    if (!(Char.IsDigit(e.KeyChar)))
                    {
                        if (e.KeyChar != (char)Keys.Back)
                        {
                            e.Handled = true;
                        }
                    }
                    break;
                case 1:
                    if (!(Char.IsLetter(e.KeyChar)))
                    {
                        if (e.KeyChar != (char)Keys.Back)
                        {
                            e.Handled = true;
                        }
                    }
                    break;
                case 2:
                    if (!(Char.IsLetter(e.KeyChar)))
                    {
                        if (e.KeyChar != (char)Keys.Back)
                        {
                            e.Handled = true;
                        }
                    }
                    break;
                case 3:
                    if (!(Char.IsLetter(e.KeyChar)))
                    {
                        if (e.KeyChar != (char)Keys.Back)
                        {
                            e.Handled = true;
                        }
                    }
                    break;
                case 4:

                    if ((!(Char.IsDigit(e.KeyChar)) && !(e.KeyChar == '-'))||((e.KeyChar == lastchar)&&(e.KeyChar=='-')))
                    {

                            if (e.KeyChar != (char)Keys.Back)
                            {
                                e.Handled = true;
                            }
                    }
                    lastchar = e.KeyChar;
                    break;
                default:
                    return;
            }
        }

        private void updat_Click(object sender, EventArgs e)
        {
            int Index = id;

            if (txtSurname.Text != "") { dataGridView1[1, Index].Value = txtSurname.Text; }
            if (txtName.Text != "") { dataGridView1[2, Index].Value = txtName.Text; }
            if (txtSecondName.Text != "") { dataGridView1[3, Index].Value = txtSecondName.Text; }
            if (txtNumber.Text != "") { dataGridView1[4, Index].Value = txtNumber.Text; }
        }
    }
}
